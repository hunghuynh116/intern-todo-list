### To-do list

1. UI
2. Create
3. Update
4. Delete
5. Mark as done
6. Search (Not)
7. Save to local storage (Not)
8. Filter

### Start JSON server

```
json-server --watch db.json --port 3333
```
