import { useSelector } from 'react-redux';

export const useTodoSelector = () => useSelector((state) => state.todo);
