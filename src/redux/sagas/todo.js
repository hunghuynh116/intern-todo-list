import { call, put, takeLatest } from 'redux-saga/effects';

import { createTodo, fetchTodos, deleteTodo, clearCompleted, toggleComplete } from '~/services';
import { todoActions } from '~/redux/slices/todoSlice';
import { toastSuccess } from '~/lib/toast';

function* handleFetchTodos() {
  try {
    const todos = yield call(fetchTodos);

    yield put(todoActions.fetchTodosSuccess(todos));
  } catch (error) {
    yield put(todoActions.fetchTodosFailed());
  }
}

function* handleCreateTodo(action) {
  try {
    const newTodo = yield call(createTodo, action.payload);

    yield put(todoActions.createTodoSuccess(newTodo));

    toastSuccess('New todo created!');
  } catch (error) {
    yield put(todoActions.createTodoFailed());
  }
}

function* handleDeleteTodo(action) {
  try {
    yield call(deleteTodo, action.payload.todoId);

    yield put(todoActions.deleteTodoSuccess({ todoId: action.payload.todoId }));

    toastSuccess('Todo delete!');
  } catch (error) {
    yield put(todoActions.deleteTodoFailed());
  }
}

function* handleClearCompleted(action) {
  try {
    yield call(clearCompleted, action.payload);

    yield put(todoActions.clearCompletedSuccess(action.payload));

    toastSuccess('Completed todos cleared!');
  } catch (error) {
    yield put(todoActions.clearCompletedFailed());
  }
}

function* handleToggleComplete(action) {
  try {
    yield call(toggleComplete, action.payload);

    yield put(todoActions.toggleCompleteSuccess(action.payload));
  } catch (error) {
    yield put(todoActions.toggleCompleteFailed());
  }
}

function* todoSaga() {
  yield takeLatest(todoActions.fetchTodosRequest, handleFetchTodos);
  yield takeLatest(todoActions.createTodoRequest, handleCreateTodo);
  yield takeLatest(todoActions.deleteTodoRequest, handleDeleteTodo);
  yield takeLatest(todoActions.clearCompletedRequest, handleClearCompleted);
  yield takeLatest(todoActions.toggleCompleteRequest, handleToggleComplete);
}

export default todoSaga;
