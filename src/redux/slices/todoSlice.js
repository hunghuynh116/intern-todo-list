import { createSlice } from '@reduxjs/toolkit';
import { removeLoading, setLoading } from '~/helpers';

const todoSlice = createSlice({
  name: 'todo',
  initialState: {
    todos: [],
    loadings: [],
    filterType: 'all', // all, active, completed
  },
  reducers: {
    fetchTodosRequest: (state) => {
      setLoading(state, 'fetchTodos');
    },
    fetchTodosSuccess: (state, action) => {
      state.todos = action.payload;

      removeLoading(state, 'fetchTodos');
    },
    fetchTodosFailed: (state) => {
      removeLoading(state, 'fetchTodos');
    },

    createTodoRequest: (state) => {
      setLoading(state, 'createTodo');
    },
    createTodoSuccess: (state, action) => {
      state.todos.unshift(action.payload);

      removeLoading(state, 'createTodo');
    },
    createTodoFailed: (state) => {
      removeLoading(state, 'createTodo');
    },

    deleteTodoRequest: (state) => {
      setLoading(state, 'deleteTodo');
    },
    deleteTodoSuccess: (state, { payload }) => {
      state.todos = state.todos.filter((todo) => todo.id !== payload.todoId);

      removeLoading(state, 'deleteTodo');
    },
    deleteTodoFailed: (state) => {
      removeLoading(state, 'deleteTodo');
    },

    clearCompletedRequest: (state) => {
      setLoading(state, 'clearCompleted');
    },
    clearCompletedSuccess: (state, { payload }) => {
      state.todos = state.todos.filter((todo) => !payload.includes(todo.id));

      removeLoading(state, 'clearCompleted');
    },
    clearCompletedFailed: (state) => {
      removeLoading(state, 'clearCompleted');
    },

    toggleCompleteRequest: (state) => {
      setLoading(state, 'toggleComplete');
    },
    toggleCompleteSuccess: (state, { payload: updatedTodo }) => {
      state.todos.forEach((todo) => {
        if (todo.id === updatedTodo.id) todo.completed = updatedTodo.completed;
      });

      removeLoading(state, 'toggleComplete');
    },
    toggleCompleteFailed: (state) => {
      removeLoading(state, 'toggleComplete');
    },

    setFilterType: (state, action) => {
      state.filterType = action.payload;
    },
  },
});

export const todoActions = todoSlice.actions;

export default todoSlice.reducer;
