import axiosClient from '~/api/axiosClient';

export const fetchTodos = () => {
  return axiosClient.get('todos');
};

export const createTodo = (newTodo) => {
  return axiosClient.post('todos', newTodo);
};

export const deleteTodo = async (todoId) => {
  axiosClient.delete('todos/' + todoId);
};

export const clearCompleted = async (todoIds) => {
  Promise.all(todoIds.map((todoId) => deleteTodo(todoId)));
};

export const toggleComplete = async (updatedTodo) => {
  axiosClient.put('todos/' + updatedTodo.id, updatedTodo);
};
