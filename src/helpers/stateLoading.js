export const setLoading = (state, loadingType) => {
  state.loadings.push(loadingType);
};

export const removeLoading = (state, loadingType) => {
  state.loadings = state.loadings.filter((loading) => loading !== loadingType);
};
