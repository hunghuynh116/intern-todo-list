const filterTodos = (todos, filterType) => {
  switch (filterType) {
    case 'all':
      return todos;
    case 'active':
      return todos.filter((todo) => !todo.completed);
    case 'completed':
      return todos.filter((todo) => todo.completed);
    default:
      throw new Error('Invalid filter type');
  }
};

export default filterTodos;
