import { memo } from 'react';

const Heading = () => {
  return <h1 className='font-bold text-3xl flex-shrink-0'>Todo List</h1>;
};

export default memo(Heading);
