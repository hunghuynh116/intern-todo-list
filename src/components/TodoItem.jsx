import { memo } from 'react';

import { XIcon, CheckIcon } from '@heroicons/react/outline';
import { useDispatch } from 'react-redux';
import clsx from 'clsx';

import { todoActions } from '~/redux/slices/todoSlice';

const TodoItem = (todo) => {
  const { id: todoId, caption, completed } = todo;

  const dispatch = useDispatch();

  const deleteTodo = () => {
    dispatch(todoActions.deleteTodoRequest({ todoId }));
  };

  const toggleComplete = () => {
    dispatch(todoActions.toggleCompleteRequest({ ...todo, completed: !todo.completed }));
  };

  return (
    <div
      className={clsx(
        'flex items-center px-2 py-2.5 rounded-md',
        'transition-all duration-150',
        completed ? ['bg-red-400', 'text-white', 'animate-flip'] : 'text-gray-600',
      )}
    >
      {completed ? (
        <CheckIcon
          onClick={toggleComplete}
          className={clsx('w-4 h-4 rounded-sm mr-2', 'text-white', 'cursor-pointer')}
        />
      ) : (
        <div
          onClick={toggleComplete}
          className={clsx('w-4 h-4 border-2 rounded-sm border-gray-500 mr-2', 'cursor-pointer')}
        />
      )}
      <p>{caption}</p>
      <XIcon onClick={deleteTodo} className={clsx('w-5 h-5 ml-auto', 'cursor-pointer')} />
    </div>
  );
};

export default memo(TodoItem);
