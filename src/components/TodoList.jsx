import { memo, useEffect, useRef } from 'react';

import { useTodoSelector } from '~/redux/selectors/todo';
import { filterTodos } from '~/helpers';

import TodoItem from './TodoItem';

const TodoList = () => {
  const { todos, filterType } = useTodoSelector();

  const listRef = useRef(null);

  useEffect(() => {
    listRef.current.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, [todos.length]);

  return (
    <div
      data-todo-list
      ref={listRef}
      className='overflow-y-auto overflow-x-hidden mt-4 -ml-4 -mr-2 pl-4 pr-2 py-2'
    >
      <div
        style={{
          perspective: '350px',
        }}
        className='space-y-3'
      >
        {filterTodos(todos, filterType).map((todo) => (
          <TodoItem key={todo.id} {...todo} />
        ))}
      </div>
    </div>
  );
};

export default memo(TodoList);
