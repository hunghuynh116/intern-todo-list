import clsx from 'clsx';

const Button = ({ children, active, ...rest }) => {
  return (
    <button
      {...rest}
      className={clsx('px-3 py-1.5 rounded-md', active && 'text-white bg-red-400', rest.className)}
    >
      {children}
    </button>
  );
};

export default Button;
