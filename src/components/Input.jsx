import { memo, useState } from 'react';

import { PencilAltIcon } from '@heroicons/react/outline';
import { useDispatch } from 'react-redux';
import { nanoid } from 'nanoid';
import clsx from 'clsx';

import { todoActions } from '~/redux/slices/todoSlice';

const Input = ({ resetTransform }) => {
  const [caption, setCaption] = useState('');

  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!caption.trim()) return;

    const newTodo = {
      id: nanoid(16),
      caption,
      completed: false,
    };

    dispatch(todoActions.createTodoRequest(newTodo));
    setCaption('');
  };

  return (
    <form
      onSubmit={handleSubmit}
      className='flex items-center mt-2 border border-gray-300 flex-shrink-0'
    >
      <input
        value={caption}
        onChange={(e) => {
          resetTransform();
          setCaption(e.target.value);
        }}
        className={clsx('px-2.5 py-2 flex-grow outline-none', 'bg-transparent')}
        placeholder='Add new todo'
      />
      <button type='submit'>
        <PencilAltIcon className={clsx('w-5 h-5 mr-2', 'text-gray-500', 'cursor-pointer')} />
      </button>
    </form>
  );
};

export default memo(Input);
