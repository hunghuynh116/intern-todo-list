import { memo } from 'react';

import { useDispatch } from 'react-redux';
import clsx from 'clsx';

import { useTodoSelector } from '~/redux/selectors/todo';
import { filterTodos } from '~/helpers';
import { todoActions } from '~/redux/slices/todoSlice';

import Button from './Button';

const Filter = () => {
  const { todos, filterType } = useTodoSelector();

  const dispatch = useDispatch();

  const activeTodos = filterTodos(todos, 'active');

  const clearCompleted = () => {
    const completedTodos = filterTodos(todos, 'completed');

    const todoIds = completedTodos.map((todo) => todo.id);

    if (todoIds.length === 0) return;

    dispatch(todoActions.clearCompletedRequest(todoIds));
  };

  return (
    <footer className={clsx('flex items-center text-sm gap-x-1 mt-4 flex-shrink-0')}>
      <span>{activeTodos.length} items left</span>

      <Button
        onClick={() => dispatch(todoActions.setFilterType('all'))}
        active={filterType === 'all'}
        className='ml-auto'
      >
        All
      </Button>
      <Button
        onClick={() => dispatch(todoActions.setFilterType('active'))}
        active={filterType === 'active'}
      >
        Active
      </Button>
      <Button
        onClick={() => dispatch(todoActions.setFilterType('completed'))}
        active={filterType === 'completed'}
      >
        Completed
      </Button>
      <Button onClick={clearCompleted}>Clear completed</Button>
    </footer>
  );
};

export default memo(Filter);
