module.exports = {
  flip: {
    '0%': {
      transform: 'rotateX(90deg)',
      opacity: 0,
    },
    '40%': {
      transform: 'rotateX(-10deg)',
    },
    '70%': {
      transform: 'rotateX(10deg)',
    },
    '100%': {
      transform: 'rotateX(0deg)',
      opacity: 1,
    },
  },

  'bg-gradient': {
    '0%': {
      'background-position': 'left',
    },
    '50%': {
      'background-position': 'right',
    },
    '100%': {
      'background-position': 'left',
    },
  },
};
