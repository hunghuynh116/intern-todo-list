import { useCallback, useEffect, useState } from 'react';
import ReactDOM from 'react-dom';

import { Toaster } from 'react-hot-toast';
import { useDispatch } from 'react-redux';
import clsx from 'clsx';

import { todoActions } from './redux/slices/todoSlice';

import Heading from './components/Heading';
import Input from './components/Input';
import TodoList from './components/TodoList';
import Filter from './components/Filter';

function App() {
  const [rotateX, setRotateX] = useState(0);
  const [rotateY, setRotateY] = useState(0);

  const dispatch = useDispatch();

  const resetTransform = useCallback(() => {
    setRotateX(0);
    setRotateY(0);
  });

  useEffect(() => {
    dispatch(todoActions.fetchTodosRequest());
  }, [dispatch]);

  useEffect(() => {
    const handleMouseMove = (e) => {
      if (e.target.closest('[data-container')) {
        resetTransform();

        return;
      }

      const rotateX = (window.innerWidth / 2 - e.pageX) / 45;
      const rotateY = (window.innerHeight / 2 - e.pageY) / 25;

      setRotateX(rotateX);
      setRotateY(rotateY);
    };

    window.addEventListener('mousemove', handleMouseMove);
    document.addEventListener('mouseleave', resetTransform);

    return () => {
      window.removeEventListener('mousemove', handleMouseMove);
      document.removeEventListener('mouseleave', resetTransform);
    };
  }, [resetTransform]);

  return (
    <>
      <main style={{ perspective: '1200px' }}>
        <div
          data-container
          style={{
            transform: `rotateX(${rotateX}deg) rotateY(${rotateY}deg)`,
            transformStyle: 'preserve-3d',
          }}
          className={clsx(
            'flex flex-col w-[480px] max-h-[85vh] mt-16 max-w-full mx-auto p-4 rounded-md shadow-md',
            'transition-all ease-linear duration-300',
            'bg-white',
          )}
        >
          <Heading />
          <Input resetTransform={resetTransform} />
          <TodoList />
          <Filter />
        </div>
      </main>

      {ReactDOM.createPortal(<Toaster />, document.getElementById('root'))}
    </>
  );
}

export default App;
