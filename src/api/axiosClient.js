import axios from 'axios';

import { API_URL } from '~/constants';

const axiosClient = axios.create({
  baseURL: API_URL,
  headers: {
    'content-type': 'application/json',
  },
});

axiosClient.interceptors.response.use((res) => {
  if (res?.data) return res.data;
  if (err) console.log('Axios err =>', err);
});

export default axiosClient;
