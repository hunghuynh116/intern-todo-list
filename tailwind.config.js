module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{js,jsx}'],
  theme: {
    fontSize: require('./src/lib/tailwind/fontSize'),
    spacing: require('./src/lib/tailwind/spacing'),
    extend: {
      lineHeight: require('./src/lib/tailwind/lineHeight'),
      keyframes: require('./src/lib/tailwind/keyframes'),
      animation: require('./src/lib/tailwind/animation'),
    },
  },
};
